**************************
Release notes and upgrades
**************************

Cumulative release notes summary on upgrade information for Ignimotion features and products.

Version 4.x.x
-------------

:Date: March 10, 2020

Before you upgrade to Ignimotion Vxx, review the upgrade information for any products you may have. 

Some products require you to complete specific tasks before you upgrade. 

+-------------------------+----------------------------------------------------------------------+
| Application & Feature   | Details                                                              |
+=========================+======================================================================+
| Filter Bar              | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx            |
+-------------------------+----------------------------------------------------------------------+
| xxx                     | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx            |
+-------------------------+----------------------------------------------------------------------+
| xxx                     | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx            |
+-------------------------+----------------------------------------------------------------------+
| xxx                     | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx            |
+-------------------------+----------------------------------------------------------------------+
| xxx                     | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx            |
+-------------------------+----------------------------------------------------------------------+


Version 4.0.x
-------------

:Date: March 10, 2020

* xxx


Version 4.0.x
-------------

:Date: March 10, 2020

* xxx
