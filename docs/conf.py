# -*- coding: utf-8 -*-
import sys
import os
import re

sys.path.append(os.path.abspath('..'))
sys.path.append(os.path.abspath('./docs/'))

from sphinx.locale import _

from sphinx_rtd_theme import __version__

from recommonmark.parser import CommonMarkParser

project = u'Ignimotion Platform'
slug = re.sub(r'\W+', '-', project.lower())
version = __version__
release = __version__
author = u'Ignimotion 2020,'
copyright = author
language = 'en'

extensions = [
    'sphinx.ext.intersphinx',
    'sphinx.ext.autodoc',
    'sphinx.ext.mathjax',
    'sphinx.ext.viewcode',
    'sphinx_rtd_theme',
]


source_suffix = ['.rst', '.md']
source_parsers = {
    '.md': CommonMarkParser,
}

exclude_patterns = []
locale_dirs = ['locale/']
gettext_compact = False
master_doc = 'index'
suppress_warnings = ['image.nonlocal_uri']
pygments_style = 'default'

intersphinx_mapping = {
    'rtd': ('https://docs.readthedocs.io/en/latest/', None),
    'sphinx': ('http://www.sphinx-doc.org/en/stable/', None),
}

html_theme = "sphinx_rtd_theme"
html_theme_path = ["_theme", ]
html_theme_options = {
    'logo_only': False,
    'style_nav_header_background': '#4e5b73',
    'prev_next_buttons_location': 'bottom'
}

html_logo = "img/logo_white.svg"
html_show_sourcelink = True

htmlhelp_basename = slug

latex_documents = [
  ('index', '{0}.tex'.format(slug), project, author, 'manual'),
]

man_pages = [
    ('index', slug, project, [author], 1)
]

texinfo_documents = [
  ('index', slug, project, author, slug, project, 'Miscellaneous'),
]

html_use_smartypants = True

# Extensions to theme docs
def setup(app):
    from sphinx.domains.python import PyField
    from sphinx.util.docfields import Field

    app.add_object_type(
        'confval',
        'confval',
        objname='configuration value',
        indextemplate='pair: %s; configuration value',
        doc_field_types=[
            PyField(
                'type',
                label=_('Type'),
                has_arg=False,
                names=('type',),
                bodyrolename='class'
            ),
            Field(
                'default',
                label=_('Default'),
                has_arg=False,
                names=('default',),
            ),
        ]
    )