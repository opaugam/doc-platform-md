*********************************
 Getting started with Platform UI
*********************************

**Access data on any device through a configurable web-based interface**

Use the default Ignimotion Platform user interface to access lists and forms, real-time form updates, user presence, an application navigator with tabs for favorites and history, and an activity stream.

Watch the five-minute video User Interface | Overview to learn about the elements of the user interface. 

xxxVideoxxx


**User Interface**

xxxImagexxx

**Components**

+-------------------------+----------------------------------------------------------------------+
| Component               | Description                                                          |
+=========================+======================================================================+
| Filter Bar              | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx            |
+-------------------------+----------------------------------------------------------------------+
| xxx                     | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx            |
+-------------------------+----------------------------------------------------------------------+
| xxx                     | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx            |
+-------------------------+----------------------------------------------------------------------+
| xxx                     | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx            |
+-------------------------+----------------------------------------------------------------------+
| xxx                     | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx            |
+-------------------------+----------------------------------------------------------------------+


.. Related tasks:: Configure logo, colors, and system defaults
