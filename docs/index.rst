.. include:: ../README.rst

.. toctree::
   :caption: Platform user interface
   :maxdepth: 1

   user_interface/navigation-and-ui
   user_interface/common-ui-elements
   user_interface/browser-support

.. toctree::
   :caption: Platform Features
   :maxdepth: 1

   features/home-features
   features/portal
   features/reporting
   features/data-collection

.. toctree::
    :maxdepth: 2
    :caption: Platform Configuration

    configuration/home-configuration
    configuration/configuration

.. toctree::
    :maxdepth: 2
    :caption: Platform Administration

    administration/home-administration

.. toctree::
    :maxdepth: 2
    :caption: Release Notes

    release/home-release
